#!/usr/bin/env python3

import json
import logging
import os
import shutil
import sys


def add_folder(src, dst):
    for path in os.listdir(src):
        src_new = os.path.join(src, path)
        dst_new = os.path.join(dst, path)

        src_new_is_dir = os.path.isdir(src_new)

        if src_new_is_dir:
            if not os.path.isdir(dst_new):
                os.mkdir(dst_new, 0o755)
            add_folder(src_new, dst_new)
        else:
            add_file(src_new, dst_new)


def add_file(src, dst):
    if os.path.exists(dst):
        if os.path.islink(dst):
            logging.info('%s exists -> unlink it', dst)
            os.unlink(dst)
        else:
            logging.info('%s exists -> remove it', dst)
            os.remove(dst)

    logging.info('Linking %s to %s', src, dst)
    os.symlink(src, dst)


def main(path='files.json'):
    with open(path, 'r') as json_file:
        files = json.load(json_file)
        dotfiles_dir = os.path.dirname(os.path.abspath(__file__))

        for file in files:
            src = os.path.join(dotfiles_dir, file[0])
            dst_folder = os.path.expanduser(file[1])

            src_is_dir = os.path.isdir(src)

            # create output folder if it doesn't exist
            if not os.path.isdir(dst_folder):
                os.mkdir(dst_folder, 0o755)

            new_dst = os.path.join(dst_folder, file[0])
            if src_is_dir:
                if not os.path.isdir(new_dst):
                    os.mkdir(new_dst, 0o755)
                add_folder(src, new_dst)
            else:
                add_file(src, new_dst)

if __name__ == '__main__':
    logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

    main()
